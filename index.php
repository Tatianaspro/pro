<?php

// Task 1

$name = "Bob";
$age = "20";
echo "Hi $name! You are $age years old." . PHP_EOL;
echo 'Hi ' . $name . '! You are ' . $age . ' years old.' . "<br>";

// Task 2

$x = 24;
$y = 17.5;
$sum = $x + $y;
echo "The sum of $x and $y is $sum" . "<br>";

// Task 3

const INT_CONST = 4;
const STRING_CONST = "four";
const FLOAT_CONST = 4.4;
echo INT_CONST . "<br>" . STRING_CONST . "<br>" . FLOAT_CONST . "<br>";

// Task 4

$name = "Vasiliy";
$admin = $name;
echo $admin . "<br>";

// Task 5

$earth = "Земля";
$customer = "Петя";

// Task 6

//$a = 2;
//$x = 1 + (a *= 2);
// Код ошибка, перед "а" нету "$"

// Task 7a

$target = 55;
If ($target = 55) {
    echo "It is right!" . "<br>";
} else echo "It is wrong" . "<br>";

// Task 7b

$target = 55;
If ($target === 55) {
    echo "It is right!" . "<br>";
} else
    echo "It is wrong" . "<br>";

//Task 7c

$lowTarget = 5;
if ($lowTarget <= 5) {
    echo "Меньше или равно 5" . "<br>";
} else
    echo "Неверно" . "<br>";

// Task 7d

$minute = 45;
if ($minute < 15) {
    echo "it is first quarter" . "<br>";
} elseif (15 <= $minute && $minute < 30) {
    echo "It is second quarter" . "<br>";
} elseif (30 <= $minute && $minute < 45) {
    echo "it is third quarter" . "<br>";
} else
    echo "It is fourth" . "<br>";

// Task 7e

$x = 2;
$y = 5;
if($x<=3||$x>10&&$y>=2&&$y<24){
    echo "True"."<br>";
} else
    echo "False"."<br>";

// Task 7f

$var = [8,1,2,3,4];
if($var[0]+$var[1]=$var[2]+$var[3]+$var[4]){
    echo "Yes!"."<br>";
} else
    echo "No!"."<br>";

// Task 8

$language = "en_GB";
If($language == "en_GB"){
    $month = ["jan","feb","Mar","Apr","May","Jun","jul","Aug","Sep","Oct","Nov","Dec"];
} else
    $month = ["Янв","Фев","Март","Апр","Май","Июнь","Июль","Авг","Сен","Окт","Ноя","Дек"];

switch($language){
    case 'en_GB':
        echo $month = ["jan","feb","Mar","Apr","May","Jun","jul","Aug","Sep","Oct","Nov","Dec"];
        break;
    case 'ru_RU':
        echo $month = ["Янв","Фев","Март","Апр","Май","Июнь","Июль","Авг","Сен","Окт","Ноя","Дек"];
        break;


}

// Task 9

$arr=["World","!","Hello"];
echo "$arr[2].' '.$arr[0].$arr[1]";

// Task 10

$responseStatuses[100]="Сообщения успешно приняты к отправке";
$responseStatuses[105]="Ошибка в фрюормате запроса";
$responseStatuses[110]="Неверная авторизация";
$responseStatuses[115]="Недопустимый адрес отправителя";
echo "$responseStatuses[105]"."<br>"."$responseStatuses[110]"."<br>";

// Task 11


$fruits["apple"]=3;
$fruits["orange"]=5;
$fruits["pear"]=10;
$sum1=$fruits["apple"]+$fruits["orange"];
$sum2=$fruits["orange"]+$fruits["pear"];
echo $sum1."<br>".$sum2;

// Task 12
//
//$arr['cms']=['joomia','wordpress','drupal'];
//$arr['colors']=['colors'["blue"]='голубой', 'colors'['red']='красный','colors'['green']='зеленый']
// echo $arr['colors']['red'].",".$arr['cms'][1]."<br>";
// echo $arr['colors']['green']."<br>".$arr['cms'][2]."<br>";
